import { history } from "../../store";

export const closeErrorModal = () => (dispatch) => {
    dispatch({type: "CLOSE_ERROR_MODAL"});
};

export const navigateTo = (page) => (dispatch, getState, { client }) => {
    history.push("/" + page);
};
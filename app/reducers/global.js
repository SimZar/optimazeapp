export default (state = {}, action) => {
    switch (action.type) {
        case "CLOSE_ERROR_MODAL":
            return {
                ...state,
                loading: false,
                error: false
            };
        case "ERROR":
            return {
                ...state,
                loading: false,
                error: true,
                message: action.error
            };
        case "START":
            return {
                ...state,
                loading: true,
                error: false
            };
        case "SUCCESS":
            return {
                ...state,
                loading: false,
                error: false
            };
        case "SET_USER":
            return {
                ...state,
                user: action.user
            };
        case "LOGOUT":
            return {
                ...state,
                user: null
            };
        default:
            return state;
    }
};
import React from "react";
import { Modal, Button, ButtonToolbar, ButtonGroup, Form } from "react-bootstrap";
import Loader from "react-loaders";

require("../../../assets/scss/loader.scss");

const ActionModal = (props) => {
    return (
        <div className="static-modal">
            <Modal show={props.show} onHide={props.onClose}>
                <Modal.Header>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>

                <Form horizontal onSubmit={(e) => props.onConfirm(e)}>
                    <Modal.Body>
                        {props.body}

                        {props.loading ?
                            <ButtonGroup vertical block>
                                <Button disabled>
                                <Loader type="ball-pulse-sync" />
                                </Button>
                            </ButtonGroup>
                             :
                            <ButtonGroup vertical block>
                                <Button type="submit" bsSize="large" bsStyle="primary">{props.confirmButtonText}</Button>
                                <Button onClick={props.onClose} bsSize="large" bsStyle="danger">{props.closeButtonText}</Button>
                            </ButtonGroup>
                        }
                    </Modal.Body>
                </Form>
            </Modal>
        </div>
    );
};

export default ActionModal;
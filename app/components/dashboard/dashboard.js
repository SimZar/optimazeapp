import React from "react";
import Loader from "react-loaders";
import * as actions from "../actions/subjectActions";
import * as globalActions from "../actions/globalActions";
import { connect } from 'react-redux';
import { ListGroup, ListGroupItem, Panel, Well, Button, Badge } from "react-bootstrap";
import ActionModal from "../modals/actionModal";
import InfoModal from "../modals/infoModal";
import AddEditSubjectForm from "./addEditSubjectForm/addEditSubjectForm";
import AddEditTaskForm from "./addEditTaskForm/addEditTaskForm";
import moment from "moment";

import "../../../assets/scss/loader.scss";
import "font-awesome/css/font-awesome.css";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: {}
        };
    }

    componentDidMount() {
        this.props.getSubjects(this.props.username);
    }

    showAddSubjectModal() {
        this.setState({
            showModal: true,
            modal: {
                title: "Add new Subject",
                body: <AddEditSubjectForm />,
                confirmText: "Add",
                closeText: "Cancel",
                confirmCallback: (e) => this.onAddSubject(e)
            }
        });
    }

    showEditSubjectModal(subject) {
        this.setState({
            showModal: true,
            modal: {
                title: "Edit Subject",
                body: <AddEditSubjectForm
                    name={subject.name}
                    code={subject.code}
                />,
                confirmText: "Save",
                closeText: "Cancel",
                confirmCallback: (e) => this.onEditSubject(e, subject.id)
            }
        })
    }

    showDeleteSubjectModal(subject) {
        this.setState({
            showModal: true,
            modal: {
                title: "Are you sure you want to delete subject " + subject.name + "?",
                body: "",
                confirmText: "Confirm",
                closeText: "Cancel",
                confirmCallback: (e) => this.onDeleteSubject(e, subject.id)
            }
        });
    }

    showEditTaskModal(task) {
        this.setState({
            showModal: true,
            modal: {
                title: "Edit Task",
                body: <AddEditTaskForm
                    description={task.description}
                    complexity={task.complexity}
                    date={moment(task.dueDate)}
                />,
                confirmText: "Save",
                closeText: "Cancel",
                confirmCallback: (e) => this.onEditTask(e, task.id)
            }
        })
    }

    showAddTaskModal(subjectId) {
        this.setState({
            showModal: true,
            modal: {
                title: "Add new Task",
                body: <AddEditTaskForm />,
                confirmText: "Add",
                closeText: "Cancel",
                confirmCallback: (e) => this.onAddTask(e, subjectId)
            }
        });
    }

    showDeleteTaskModal(task) {
        this.setState({
            showModal: true,
            modal: {
                title: "Are you sure you want to delete task " + task.description + " ?",
                body: "",
                confirmText: "Confirm",
                closeText: "Cancel",
                confirmCallback: (e) => this.onDeleteTask(e, task.id)
            }
        });
    }

    onAddSubject(e) {
        e.preventDefault();
        return this.props.addNewSubject({
            name: e.target["formHorizontalSubjectName"].value,
            code: e.target["formHorizontalSubjectCode"].value
        }, this.props.username)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    onAddTask(e, subjectId) {
        e.preventDefault();
        return this.props.addNewTask({
            description: e.target["formHorizontalTaskDescription"].value,
            complexity: e.target["formHorizontalTaskComplexity"].value,
            dueDate: moment(e.target["formHorizontalTaskDate"].value).format("L")
        }, subjectId)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    onEditSubject(e, id) {
        e.preventDefault();
        return this.props.updateSubject({
            name: e.target["formHorizontalSubjectName"].value,
            code: e.target["formHorizontalSubjectCode"].value
        }, id)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    onDeleteSubject(e, id) {
        e.preventDefault();
        return this.props.deleteSubject(id)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    onEditTask(e, id) {
        e.preventDefault();
        return this.props.updateTask({
            description: e.target["formHorizontalTaskDescription"].value,
            complexity: e.target["formHorizontalTaskComplexity"].value,
            dueDate: moment(e.target["formHorizontalTaskDate"].value).format("L")
        }, id)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    onDeleteTask(e, id) {
        e.preventDefault();
        return this.props.deleteTask(id)
            .then(() => {
                this.setState({showModal: false});
                return this.props.error ? null : this.props.getSubjects(this.props.username);
            });
    }

    resolveComplexity(complexity) {
        switch (complexity) {
            case 0:
            case 1:
                return "success";
            case 2:
            case 3:
                return "info";
            case 4:
            case 5:
                return "warning";
            default:
                return "danger";
        }
    }

    render() {
        const subjectsList = this.props.subjects ?
            this.props.subjects.map((subject) =>
                <Well key={subject.id}>
                    <ListGroup key={subject.id}>
                        <Panel>{subject.name} {subject.code}
                            <Badge pullRight onClick={() => this.showDeleteSubjectModal(subject)}><i className="fa fa-trash fa-2x" aria-hidden="true"/></Badge>
                            <Badge pullRight onClick={() => this.showEditSubjectModal(subject)}><i className="fa fa-pencil fa-2x" aria-hidden="true"/></Badge>
                        </Panel>
                        {subject.tasks.map((task) => {
                                const complexityColour = this.resolveComplexity(task.complexity);
                                return (
                                    <ListGroupItem key={task.id} bsStyle={complexityColour}>
                                        <Panel>
                                            <Badge pullRight onClick={() => this.showDeleteTaskModal(task)}><i className="fa fa-trash" aria-hidden="true"/></Badge>
                                            <Badge pullRight onClick={() => this.showEditTaskModal(task)}><i className="fa fa-pencil" aria-hidden="true"/></Badge>
                                            <div>{task.description}</div>
                                            <div>{moment(task.dueDate).format("ll")}</div>
                                        </Panel>
                                    </ListGroupItem>
                                );
                            }
                        )}
                        <Button block onClick={() => this.showAddTaskModal(subject.id)}>Add new Task</Button>
                    </ListGroup>
                </Well>

            ) : "";

        return(
            <div>
                {!this.props.loading ? <div>
                        {subjectsList}
                        <Button block onClick={() => this.showAddSubjectModal()}>
                            <Well>
                                Add new Subject
                            </Well>
                        </Button>
                    </div>
                    : <Button disabled>
                        <Loader type="ball-pulse-sync" />
                    </Button>}
                <ActionModal
                    show={this.state.showModal}
                    onClose={() => this.setState({showModal: false})}
                    title={this.state.modal.title}
                    body={this.state.modal.body}
                    confirmButtonText={this.state.modal.confirmText}
                    closeButtonText={this.state.modal.closeText}
                    onConfirm={(e) => this.state.modal.confirmCallback(e)}
                    loading={this.props.loadingModal}
                />
                <InfoModal
                    show={this.props.error}
                    title="Error"
                    body="Something went wrong, please try again later."
                    onClose={this.props.closeErrorModal}
                    closeButtonText="Close"
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loading: state.global.loading,
    error: state.global.error,
    username: state.global.user.userName,
    subjects: state.userData.subjects,
    loadingModal: state.actionModal.loadingModal
});

const mapDispatchToProps = dispatch => ({
    getSubjects: username => dispatch(actions.getSubjects(username)),
    closeErrorModal: () => dispatch(globalActions.closeErrorModal()),
    addNewSubject: (data, username) => dispatch(actions.addNewSubject(data, username)),
    deleteSubject: (id) => dispatch(actions.deleteSubject(id)),
    deleteTask: (id) => dispatch(actions.deleteTask(id)),
    updateSubject: (data, id) => dispatch(actions.updateSubject(data, id)),
    updateTask: (data, id) => dispatch(actions.updateTask(data, id)),
    addNewTask: (data, id) => dispatch(actions.addNewTask(data, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

export default (state = {}, action) => {
    switch (action.type) {
        case "START_IN_MODAL":
            return {
                ...state,
                loadingModal: true
            };
        case "SUCCESS_IN_MODAL":
            return {
                ...state,
                loadingModal: false,
                error: false
            };
        case "ERROR_IN_MODAL":
            return {
                ...state,
                loadingModal: false,
                error: true
            };
        default:
            return state;
    }
};
import Promise from "bluebird";
import request from "superagent";
// const baseUrl = "http://taskreminder.apphb.com/api/";
const baseUrl = "http://localhost:50918/api/";
let token;

export default class HttpClientWrapper {

    get(url) {
        return new Promise((resolve, reject) => {
            request
                .get(baseUrl + url)
                .set("Authorization", "Bearer " + token)
                .withCredentials()
                .timeout({
                    response: 50000,
                    deadline: 60000,
                })
                .end((err, res) => {
                    if (err && err.timeout) {
                        reject(Error("Timed out"));
                    } else if (res.status >= 200 && res.status < 300) {
                        resolve(res.body);
                    } else {
                        reject(Error("Something is not right."));
                    }
                });
        });
    }

    post(url, data) {
        return new Promise((resolve, reject) => {
            request
                .post(baseUrl + url)
                .set("Authorization", "Bearer " + token)
                .withCredentials()
                .send(data)
                .timeout({
                    response: 50000,
                    deadline: 60000,
                })
                .end((err, res) => {
                    if (err && err.timeout) {
                        reject(Error("Timed out"));
                    } else if (res.status === 204 || res.status === 200) {
                        if (res.body && res.body.token) {
                            token = res.body.token;
                        }
                        resolve();
                    } else {
                        if (res.body) {
                            if (res.body.length > 1) {
                                reject(Error(res.body.join("\n")));
                            }
                            reject(Error(res.body))
                        }

                        reject(Error("Something went wrong."));
                    }
                });
        });
    }

    put(url, data) {
        return new Promise((resolve, reject) => {
            request
                .put(baseUrl + url)
                .set("Authorization", "Bearer " + token)
                .withCredentials()
                .send(data)
                .timeout({
                    response: 50000,
                    deadline: 60000,
                })
                .end((err, res) => {
                    if (err && err.timeout) {
                        reject(Error("Timed out"));
                    } else if (res.status === 204 || res.status === 200) {
                        resolve();
                    } else {
                        reject(Error("Something is not right."));
                    }
                });
        });
    }

    delete(url, data) {
        return new Promise((resolve, reject) => {
            request
                .del(baseUrl + url)
                .set("Authorization", "Bearer " + token)
                .withCredentials()
                .send(data)
                .timeout({
                    response: 50000,
                    deadline: 60000,
                })
                .end((err, res) => {
                    if (err && err.timeout) {
                        reject(Error("Timed out"));
                    } else if (res.status === 204 || res.status === 200) {
                        resolve();
                    } else {
                        reject(Error("Something is not right."));
                    }
                });
        });
    }
}
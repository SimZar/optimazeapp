const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const appDir = path.resolve(__dirname, "app");
const buildDir = path.resolve(__dirname, "dist");

module.exports = {
    entry: {
        main: path.resolve(appDir, "index.js"),
    },
    output: {
        filename: "bundle.js",
        path: buildDir,
        publicPath: "/"
    },
    plugins: [new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "pages/index.html")
    })],

    module: {
        loaders: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query:  {
                    presets:  ["es2015", "react", "stage-0"],
                },
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader" // creates style nodes from JS strings
                    }, {
                        loader: "css-loader" // translates CSS into CommonJS
                    }, {
                        loader: "sass-loader" // compiles Sass to CSS
                    }
                ]
            },
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(png|gif|jpe?g)$/,
                loader: "file-loader?outputPath=content/"
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=[a-z0-9]\.[a-z0-9]\.[a-z0-9])?$/,
                loader: "url-loader?limit=100000&outputPath=content/"
            },
        ],
    },

    devtool: "source-map",

    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 9000,
        historyApiFallback: true
    }
};


import React from "react";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import * as actions from "../actions/userActions";
import * as globalActions from "../actions/globalActions";
import { connect } from 'react-redux';
import LoaderModal from "../modals/loaderModal";


class MainPageContainer extends React.Component {
    load = () => <LoaderModal />;
    render() {
        return (
            <div>
                <Navbar collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            OptiMaze
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight>
                            {!this.props.user && !document.location.href.includes("register") && <NavItem onClick={() => this.props.navigateTo("register")}>Register</NavItem>}
                            {document.location.href.includes("register") && <NavItem onClick={() => this.props.navigateTo("login")}>Login</NavItem>}
                            {this.props.user && <NavItem disabled>Welcome, {this.props.user.firstName + " " + this.props.user.lastName}</NavItem>}
                            {this.props.user && <NavItem onClick={() => this.props.logout()}>Logout</NavItem>}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                {this.props.loading? this.load() : this.props.children}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    loading: state.global.loading,
    error: state.global.error,
    user: state.global.user
});

const mapDispatchToProps = dispatch => ({
    navigateTo: page => dispatch(globalActions.navigateTo(page)),
    logout: () => dispatch(actions.logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPageContainer);
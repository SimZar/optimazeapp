import React from "react";
import { Modal, Button } from "react-bootstrap";

const styles = {
    fontFamily: "Play"
};

const InfoModal = (props) => {
    return (
        <div className="static-modal">
            <Modal show={props.show} onHide={props.onClose}>
                <Modal.Header style={styles}>
                    <Modal.Title>{props.title}</Modal.Title>
                </Modal.Header>

                <Modal.Body style={styles}>
                    {props.body}
                </Modal.Body>

                <Modal.Footer style={styles}>
                    <Button onClick={props.onClose}>{props.closeButtonText}</Button>
                </Modal.Footer>

            </Modal>
        </div>
    );
};

export default InfoModal;

import { Provider } from "react-redux";
import store, { history } from "./store";
import { render } from "react-dom";
import { Router } from "react-router";
import routes from "./routes/router-config";
import React from "react";

require("../assets/scss/common.scss");

render((
    <Provider store={store}><Router history={history} routes={routes} /></Provider>
), document.getElementById("app"));
import React from "react";
import { Form, FormGroup, Col, FormControl, Button, ControlLabel, Well} from "react-bootstrap";
import { connect } from 'react-redux';
import * as actions from "../actions/userActions";
import * as globalActions from "../actions/globalActions";
import Loader from "react-loaders";
require("../../../assets/scss/loader.scss");
import InfoModal from "../modals/infoModal";

class Login extends React.Component{
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.submitButton = this.submitButton.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        return this.props.login({
            username: e.target["formHorizontalUsername"].value,
            password: e.target["formHorizontalPassword"].value
        });
    }

    submitButton() {
        return this.props.loading ?
            (<Button>
                <Loader type="ball-pulse-sync" />
            </Button>) : (
                <Button type="submit">
                    Log in
                </Button>
            )
    }

    render(){
        return(
            <Col md={6} mdOffset={3}>
                <Well>
                <Form horizontal onSubmit={this.onSubmit}>
                    <FormGroup controlId="formHorizontalUsername">
                        <Col componentClass={ControlLabel} sm={2}>
                            Username
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="Username" />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={2}>
                            Password
                        </Col>
                        <Col sm={10}>
                            <FormControl type="password" placeholder="Password" />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button type="submit">
                                Log in
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
                <InfoModal
                    show={this.props.error || false}
                    title="Error"
                    body="Invalid Username or Password"
                    onClose={this.props.closeErrorModal}
                    closeButtonText="Close"
                />
                </Well>
            </Col>
        );
    }
};

const mapStateToProps = state => ({
    loading: state.global.loading,
    error: state.global.error
});

const mapDispatchToProps = dispatch => ({
    login: credentials => dispatch(actions.login(credentials)),
    closeErrorModal: () => dispatch(globalActions.closeErrorModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
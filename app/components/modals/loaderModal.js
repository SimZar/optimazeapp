import React from "react";
import Loader from "react-loaders";
require("../../../assets/scss/loader.scss");

const loaderModal = () => {
    return (
        <div className="overlay">
            <div className="loader-centered">
                    <Loader type="square-spin" />
            </div>
        </div>
    );
};

export default loaderModal;

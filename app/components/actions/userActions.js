import { history } from "../../store";

export const login = (credentials) => (dispatch, getState, { client }) => {
    dispatch({type: "START"});
    return client.post("users/login", credentials)
        .then(() => client.post("users/token", credentials))
        .then(() => client.get("users/me"))
        .then((user) => {
            dispatch({type: "SET_USER", user});
            dispatch({type: "SUCCESS"});
            return history.push("/dashboard");
        })
        .catch((error) => dispatch({type: "ERROR", error}));
};

export const register = (data) => (dispatch, getState, { client }) => {
    dispatch({type: "START"});
    client.post("users/register", data)
        .then(() => {
            dispatch({type: "SUCCESS"});
            history.push("/login");
        })
        .catch((error) => {
        return dispatch({type: "ERROR", error: error.message});
    });
};

export const logout = () => (dispatch, getState, { client }) => {
    dispatch({type: "START"});
    client.post("users/logout")
        .then(() => {
            history.push("/login");
            dispatch({type: "SUCCESS"});
            dispatch({type: "LOGOUT"});
        })
        .catch((error) => dispatch({type: "ERROR", error}));
};

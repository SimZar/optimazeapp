import React from "react";
import { Route, IndexRoute } from "react-router";

import PageContainer from "../components/page-header/pageHeader";
import Login from "../components/login/login";
import Register from "../components/register/register";
import Dashboard from "../components/dashboard/dashboard";

const routes =
    (<Route path="/" component={PageContainer} >
        <IndexRoute component={Login} />
        <Route path="login" component={Login} />
        <Route path="dashboard" component={Dashboard} />
        <Route path="register" component={Register} />
    </Route>)
;

export default routes;


export const getSubjects = (username) => (dispatch, getState, { client }) => {
    dispatch({type: "START"});
    return client.get("subjects/user/" + username)
        .then((subjects) => {
            dispatch({type: "SUCCESS"});
            return dispatch({type: "SET_SUBJECTS", subjects});
        })
        .catch((error) => dispatch({type: "ERROR", error: error.message}));
};

export const addNewSubject = (data, username) => (dispatch, getState, { client }) => {
    dispatch({type: "START_IN_MODAL"});
    return client.post("subjects/" + username, data)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });};

export const deleteSubject = (id) => (dispatch, getState, { client}) => {
    dispatch({type: "START_IN_MODAL"});
    return client.delete("subjects/" + id)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });};

export const updateSubject = (data, id) => (dispatch, getState, { client }) => {
    dispatch({type: "START_IN_MODAL"});
    return client.put("subjects/" + id, data)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });};

export const addNewTask = (data, subjectId) => (dispatch, getState, { client }) => {
    dispatch({type: "START_IN_MODAL"});
    return client.post("tasks/" + subjectId, data)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });};

export const deleteTask = (id) => (dispatch, getState, { client}) => {
    dispatch({type: "START_IN_MODAL"});
    return client.delete("tasks/" + id)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });};

export const updateTask = (data, id) => (dispatch, getState, { client }) => {
    dispatch({type: "START_IN_MODAL"});
    return client.put("tasks/" + id, data)
        .then(() => dispatch({type: "SUCCESS_IN_MODAL"}))
        .catch((error) => {
            dispatch({type: "ERROR", error: error.message});
            dispatch({type: "ERROR_IN_MODAL", error: error.message});
        });
};
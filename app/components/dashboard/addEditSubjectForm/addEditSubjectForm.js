import React from "react";
import { Well, FormGroup, Col, FormControl, ControlLabel } from "react-bootstrap";

const AddEditSubjectForm = (props) => {
    return (
        <Well>
            <FormGroup controlId="formHorizontalSubjectName">
                <Col componentClass={ControlLabel} sm={2}>
                    Subject Name
                </Col>
                <Col sm={10}>
                    <FormControl type="text" placeholder="Subject Name" defaultValue={props.name} />
                </Col>
            </FormGroup>

            <FormGroup controlId="formHorizontalSubjectCode">
                <Col componentClass={ControlLabel} sm={2}>
                    Subject Code
                </Col>
                <Col sm={10}>
                    <FormControl type="text" placeholder="Subject Code" defaultValue={props.code} />
                </Col>
            </FormGroup>
        </Well>
    );
};

export default AddEditSubjectForm;
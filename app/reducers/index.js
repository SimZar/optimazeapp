import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";

import global from "./global";
import userData from "./userData";
import actionModal from "./actionModal";

const rootReducer = combineReducers({
    global,
    userData,
    actionModal,
    routing: routerReducer
});

export default rootReducer;

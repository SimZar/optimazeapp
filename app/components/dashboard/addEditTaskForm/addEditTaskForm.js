import React from "react";
import { Well, FormGroup, Col, FormControl, ControlLabel, Checkbox } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class AddEditTaskForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            date: props.date ? props.date : null
        };
    }

    render() {
        return (
            <Well>
                <FormGroup controlId="formHorizontalTaskDescription">
                    <Col componentClass={ControlLabel} sm={2}>
                        Description
                    </Col>
                    <Col sm={10}>
                        <FormControl type="text" placeholder="Description" defaultValue={this.props.description}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalTaskComplexity">
                    <Col componentClass={ControlLabel} sm={2}>
                        Complexity
                    </Col>
                    <Col sm={10}>
                        <FormControl type="number" placeholder="Complexity" defaultValue={this.props.complexity}/>
                    </Col>
                </FormGroup>

                <FormGroup controlId="formHorizontalTaskDate">
                    <Col componentClass={ControlLabel} sm={2}>
                        Due Date
                    </Col>
                    <Col sm={10}>
                        <DatePicker
                            id="formHorizontalTaskDate"
                            className="form-control"
                            selected={this.state.date}
                            onSelect={(date) => this.setState({date})}
                            placeholderText="Due Date"
                        />
                    </Col>
                </FormGroup>
            </Well>
        );
    }
};

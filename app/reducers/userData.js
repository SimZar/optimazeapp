export default (state = {}, action) => {
    switch (action.type) {
        case "SET_SUBJECTS":
            return {
                ...state,
                subjects: action.subjects
            };
        default:
            return state;
    }
};
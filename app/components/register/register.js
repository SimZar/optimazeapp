import React from "react";
import { Form, FormGroup, Col, FormControl, Button, ControlLabel, Image} from "react-bootstrap";
import { connect } from 'react-redux';
import * as actions from "../actions/userActions";
import * as globalActions from "../actions/globalActions";
import Loader from "react-loaders";
require("../../../assets/scss/loader.scss");
import InfoModal from "../modals/infoModal";

class Register extends React.Component{
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
        this.submitButton = this.submitButton.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.register({
            username: e.target["formHorizontalUsername"].value,
            password: e.target["formHorizontalPassword"].value,
            email: e.target["formHorizontalEmail"].value,
            firstname: e.target["formHorizontalFirstname"].value,
            lastname: e.target["formHorizontalLastname"].value
        });
    }

    submitButton() {
        return this.props.loading ?
            (<Button>
                <Loader type="ball-pulse-sync" />
            </Button>) : (
                <Button type="submit">
                    Register
                </Button>
            )
    }

    render(){
        return(
            <div>
                <Form horizontal onSubmit={this.onSubmit}>
                    <FormGroup controlId="formHorizontalUsername">
                        <Col componentClass={ControlLabel} sm={2}>
                            Username
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="Username" />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={2}>
                            Password
                        </Col>
                        <Col sm={10}>
                            <FormControl type="password" placeholder="Password" />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={2}>
                            Email
                        </Col>
                        <Col sm={10}>
                            <FormControl type="email" placeholder="Email" />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalFirstname">
                        <Col componentClass={ControlLabel} sm={2}>
                            First Name
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="First Name" />
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalLastname">
                        <Col componentClass={ControlLabel} sm={2}>
                            Last Name
                        </Col>
                        <Col sm={10}>
                            <FormControl type="text" placeholder="Last Name" />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            {this.submitButton()}
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Image
                                src="http://s2.quickmeme.com/img/a5/a587f7f895aadf9976c4290e5d0c4a2be77d68ce1af9367fcb407a305edf74f8.jpg"
                                responsive
                            />
                        </Col>
                    </FormGroup>

                </Form>
                <InfoModal
                    show={this.props.error || false}
                    title="Error"
                    body={this.props.message}
                    onClose={this.props.closeErrorModal}
                    closeButtonText="Close"
                />
            </div>
        );
    }
};

const mapStateToProps = state => ({
    loading: state.global.loading,
    error: state.global.error,
    message: state.global.message
});

const mapDispatchToProps = dispatch => ({
    register: data => dispatch(actions.register(data)),
    closeErrorModal: () => dispatch(globalActions.closeErrorModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);